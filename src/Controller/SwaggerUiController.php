<?php

namespace Drupal\ic_swagger_ui\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Response;

/**
 * Deal with Swagger UI and OpenAPI.
 */
class SwaggerUiController extends ControllerBase {

  /**
   * Show the documentation in the Swagger UI.
   */
  public function show() {
    return [];
  }

  /**
   * Scan the Immunoconnect API annotations and provide json.
   */
  public function scan() {
    $pathToApi = realpath(
      $this->moduleHandler()->getModule('ic_api')->getPath() . '/src'
    );
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $response->setContent(\Swagger\scan($pathToApi));

    return $response;
  }

}
