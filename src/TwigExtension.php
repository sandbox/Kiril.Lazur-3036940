<?php

namespace Drupal\ic_swagger_ui;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Twig_Extension;

/**
 * Twig extension to handle needs of ic_swagger_ui module.
 */
class TwigExtension extends Twig_Extension {

  /**
   * Instance of module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * TwigExtension constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Service module_handler to get module information.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Get the extension name.
   *
   * @inheritdoc
   *
   * @return string
   *   The string with the name.
   */
  public function getName() {
    return 'ic_swagger_ui_twig_extension';
  }

  /**
   * Get list of functions of ic_swagger_ui twig extension.
   *
   * @inheritdoc
   *
   * @return array
   *   The array with functions.
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction(
        'getPathToImages',
        [
          $this,
          'getPathToImages',
        ],
        [
          'is_safe' => ['html'],
        ]
      ),
    ];
  }

  /**
   * Get path to the module images directory.
   *
   * @inheritdoc
   *
   * @return string
   *   e.g. /sites/msd_myph_ch/modules/custom/ic_swagger_ui/images
   */
  public function getPathToImages() {
    return DIRECTORY_SEPARATOR
      . $this->moduleHandler->getModule('ic_swagger_ui')->getPath()
      . DIRECTORY_SEPARATOR
      . 'images';
  }

}
