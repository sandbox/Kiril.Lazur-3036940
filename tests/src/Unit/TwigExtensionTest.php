<?php

namespace Drupal\Tests\ic_swagger_ui\Unit;

use Drupal\ic_swagger_ui\TwigExtension;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for QuestionnaireManager class.
 *
 * @group ic
 */
class TwigExtensionTest extends UnitTestCase {

  /**
   * TwigExtension.
   *
   * @var \Drupal\ic_swagger_ui\TwigExtension
   */
  protected $twigExtension;

  /**
   * ModuleHandler mock.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $moduleHandler;

  /**
   * Module mock.
   *
   * @var \Drupal\Core\Extension\Extension|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $module;

  /**
   * Set up SUT and mocks.
   *
   * @inheritdoc
   */
  protected function setUp() {
    $this->module = $this->getMockBuilder('\Drupal\Core\Extension\Extension')
      ->disableOriginalConstructor()
      ->getMock();
    $moduleInterfacePath = 'Drupal\Core\Extension\ModuleHandlerInterface';
    $this->moduleHandler = $this->getMock($moduleInterfacePath);
    $this->twigExtension = new TwigExtension($this->moduleHandler);
  }

  /**
   * Test that getFunctions method returns an array.
   */
  public function testGetFunctionsReturnsArray() {
    $functions = $this->twigExtension->getFunctions();
    $this->assertInternalType('array', $functions);
  }

  /**
   * Test that getFunctions returns an array with at least one function.
   */
  public function testGetFunctionReturnsArrayWithAtLeastOneTwigSimpleFunction() {
    $functions = $this->twigExtension->getFunctions();
    $this->assertInstanceOf('Twig_SimpleFunction', $functions[0]);
  }

  /**
   * Test that getFunctions returns an array with getPathToImages function.
   */
  public function testGetFunctionReturnsArrayWithGetPathToImagesAsFirstElement() {
    $functions = $this->twigExtension->getFunctions();
    /** @var \Twig_SimpleFunction $getPathToImagesFunction */
    $getPathToImagesFunction = $functions[0];
    $this->assertSame('getPathToImages', $getPathToImagesFunction->getName());
  }

  /**
   * Test that getPathToImages returns string.
   */
  public function testGetPathToImagesReturnsString() {
    $this->moduleHandler->expects($this->once())->method('getModule')
      ->willReturn($this->module);
    $pathToImages = $this->twigExtension->getPathToImages();
    $this->assertInternalType('string', $pathToImages);
  }

  /**
   * Test that getPathToImages returns valid path.
   */
  public function testGetPathToImagesReturnsValidPathToImages() {
    $this->module->expects($this->once())->method('getPath')
      ->willReturn('sites/msd_myph_ch/modules/custom/ic_swagger_ui');
    $this->moduleHandler->expects($this->once())->method('getModule')
      ->willReturn($this->module);
    $pathToImages = $this->twigExtension->getPathToImages();
    $expectedPathToImages = DIRECTORY_SEPARATOR
      . 'sites/msd_myph_ch/modules/custom/ic_swagger_ui'
      . DIRECTORY_SEPARATOR . 'images';
    $this->assertSame(
      $expectedPathToImages,
      $pathToImages
    );
  }

}
